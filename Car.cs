using System;

namespace Filmpjes {
    class Car {
        public float Speed;

        public float GetSpeed() {
            return Speed;
        }

        public void Accelerate() {
            if(Speed < 10) {
                Speed = 10;
            }
            else {
                Speed += (120 - GetSpeed()) / 15;
            }
        }

        public void Break() {
            if(Speed < 10) {
                Speed = 0;
            }
            else {
                Speed = 15f / 16f * GetSpeed() - 7.5f;
            }
        }

        public static void DemonstrateCar() {
            Car car = new Car();
            for(int i = 0; i < 50; i++) {
                car.Accelerate();
                Console.WriteLine($"De snelheid is: {car.GetSpeed()}");
            }
            for(int i = 0; i < 50; i++) {
                car.Break();
                Console.WriteLine($"De snelheid is: {car.GetSpeed()}");
            }
        }
    }
}